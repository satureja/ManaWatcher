#ifndef USERRELATIONSRESOLVER_H
#define USERRELATIONSRESOLVER_H

#include "definitions.h"

#include <QFileSystemWatcher>
#include <QObject>

class RelationResolver : public QObject
{
    Q_OBJECT
public:
    explicit RelationResolver(QObject * parent);

    void setPath(QString path);

public slots:
    void read();

signals:
    void enemiesResolved(PlayerList);
    void friendsResolved(PlayerList);

private slots:
    void waitAndRead();

private:
    QFileSystemWatcher watcher;

    QString path;
};

#endif // USERRELATIONSRESOLVER_H
