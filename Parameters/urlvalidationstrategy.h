#ifndef URLVALIDATIONSTRATEGY_H
#define URLVALIDATIONSTRATEGY_H

#include <QString>

class UrlValidationStrategy
{
public:
    bool check(const QString & url) const;
};

#endif // URLVALIDATIONSTRATEGY_H
