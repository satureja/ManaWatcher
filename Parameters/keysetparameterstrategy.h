#ifndef KEYSETPARAMETERSTRATEGY_H
#define KEYSETPARAMETERSTRATEGY_H

#include <initializer_list>
#include <utility>
#include <QMap>
#include <QSettings>

template<typename Type, typename Storage = QSettings>
class KeySetParameterStrategy
{
public:
    KeySetParameterStrategy(std::initializer_list<std::pair<Type, QString>> data):
        data(data)
    {
    }

    KeySetParameterStrategy(KeySetParameterStrategy&& that):
        data(std::move(that.data))
    {
    }

    Type load(Storage & settings, const QString & name, const Type& defaultValue) const
    {
        if (!settings.contains(name))
            return defaultValue;

        QString rawData = settings.value(name).toString();

        return data.key(rawData, defaultValue);
    }

    bool save(Storage & settings, const QString & name, const Type& value)
    {
        if(!data.contains(value))
            return false;

        settings.setValue(name, data.value(value));
        return true;
    }

private:
    QMap<Type, QString> data;
};

#endif // KEYSETPARAMETERSTRATEGY_H
