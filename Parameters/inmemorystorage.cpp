#include "inmemorystorage.h"

void InMemoryStorage::clear()
{
    this->storage.clear();
}

bool InMemoryStorage::contains(const QString &key) const
{
    return this->storage.contains(key);
}

void InMemoryStorage::setValue(const QString &key, const QVariant &value)
{
    this->storage[key] = value;
}

QVariant InMemoryStorage::value(const QString &key, const QVariant &defaultValue) const
{
    return this->storage.value(key, defaultValue);
}
