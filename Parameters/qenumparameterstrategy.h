#ifndef QENUMPARAMETERSTRATEGY_H
#define QENUMPARAMETERSTRATEGY_H

#include <QSettings>
#include <QString>
#include <QVariant>
#include <QMetaEnum>

#include <type_traits>

template<typename Type,
         typename Storage = QSettings,
         typename = std::enable_if_t<QtPrivate::IsQEnumHelper<Type>::Value>>
class QEnumParameterStrategy
{
public:
    Type load(Storage & settings, const QString & name, const Type& defaultValue) const
    {
        QMetaEnum mo = QMetaEnum::fromType<Type>();
        QString key = settings.value(name, "").toString();

        if (key.isEmpty())
            return defaultValue;

        bool ok = false;
        int value = mo.keyToValue(key.toStdString().c_str(), &ok);

        if (!ok)
            value = defaultValue;

        return static_cast<Type>(value);
    }

    bool save(Storage & settings, const QString & name, const Type& value)
    {
        QMetaEnum mo = QMetaEnum::fromType<Type>();
        Q_ASSERT(mo.isValid());
        settings.setValue(name, mo.valueToKey(value));
        return true;
    }
};

#endif // QENUMPARAMETERSTRATEGY_H
