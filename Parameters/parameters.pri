QT += core
CONFIG += c++14

HEADERS += \
    $$PWD/parameter.h \
    $$PWD/defaultparameterstrategy.h \
    $$PWD/qenumparameterstrategy.h \
    $$PWD/keysetparameterstrategy.h \
    $$PWD/urlvalidationstrategy.h \
    $$PWD/fileexistsvalidationstrategy.h \
    $$PWD/inmemorystorage.h \

SOURCES += \
    $$PWD/urlvalidationstrategy.cpp \
    $$PWD/fileexistsvalidationstrategy.cpp \
    $$PWD/inmemorystorage.cpp \


INCLUDEPATH += $$PWD
