#include "auxiliary.h"

namespace auxiliary
{

QTextStream& operator<<(QTextStream & out, const QStringList & list)
{
    for (auto & line: list)
        out << line << "\n";
    return out;
}

std::string makeErrorMessage(const QStringList & got, const QStringList & expected)
{
    QString errorMessage;
    QTextStream out(&errorMessage);
    out << "\n" << "=== got:\n" << got << "\n" << "=== expected:\n" << expected << "\n";
    return errorMessage.toStdString();
}

} //namespace auxiliary
