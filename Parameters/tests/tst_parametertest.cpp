#include <QString>
#include <QtTest>
#include <QSettings>

#include "parameter.h"
#include "qenumparameterstrategy.h"
#include "keysetparameterstrategy.h"
#include "urlvalidationstrategy.h"
#include "fileexistsvalidationstrategy.h"
#include "mockintstrategy.h"
#include "mockintvalidationstrategy.h"
#include "auxiliary.h"

using namespace auxiliary;

class ParameterTest : public QObject
{
    Q_OBJECT

public:
    ParameterTest();

private Q_SLOTS:
    void init();
    void cleanupTestCase();

    void passRightArgumentsToTheLoadMethod();
    void passRightArgumentsToTheSaveMethod();

    void returnValueFromTheLoadMethod_data();
    void returnValueFromTheLoadMethod();

    void returnResultFromTheSaveMethod_data();
    void returnResultFromTheSaveMethod();

    void defaultStrategySave();
    void defaultStrategyLoad();
    void defaultStrategyLoadNonexistent();

    void defaultStrategyLoadWithCheck_data();
    void defaultStrategyLoadWithCheck();

    void defaultStrategySaveWithCheck_data();
    void defaultStrategySaveWithCheck();

    void qenumStrategySave();
    void qenumStrategyLoad();
    void qenumStrategyLoadNonexistent();
#if defined(QT_NO_DEBUG) && !defined(QT_FORCE_ASSERTS)
    void qenumStrategyLoadWrong();
#endif

    void keySetStrategySave_data();
    void keySetStrategySave();
    void keySetStrategyLoad_data();
    void keySetStrategyLoad();

    void urlValidationStrategy_data();
    void urlValidationStrategy();

    void fileExistsValidationStrategy_data();
    void fileExistsValidationStrategy();
    void fileExistsValidationStrategy_exists();
private:
    QStringList readIniFile() const;
    void writeIniFile(const QStringList & list) const;
    const QString iniFile;
};

ParameterTest::ParameterTest()
    : iniFile(QDir::currentPath() + QDir::separator() + "test.ini")
{
}

void ParameterTest::init()
{
    if (QFile::exists(this->iniFile))
    {
        QVERIFY(QFile::remove(this->iniFile));
    }
}

void ParameterTest::cleanupTestCase()
{
    if (QFile::exists(this->iniFile))
    {
        QVERIFY(QFile::remove(this->iniFile));
    }
}

QStringList ParameterTest::readIniFile() const
{
    QStringList result;

    if(!QFile::exists(this->iniFile))
        return result;

    QFile file(this->iniFile);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return result;

    QTextStream in(&file);
    QString line = in.readLine();
    while (!line.isNull())
    {
        result.push_back(line);
        line = in.readLine();
    }

    return result;
}

void ParameterTest::writeIniFile(const QStringList & list) const
{
    QFile file(this->iniFile);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << list;
}

void ParameterTest::passRightArgumentsToTheLoadMethod()
{
    QSettings settings("test.ini", QSettings::IniFormat);

    using TestParameter = Parameter<int, MockIntStrategy>;

    MockIntStrategy::Info info;
    const QString name = "ParamName";
    const int defaultValue = 13;

    TestParameter param(settings,
                        name,
                        defaultValue,
                        MockIntStrategy(info, 0, false));

    param();

    QCOMPARE(info.settings, &settings);
    QCOMPARE(info.name, name);
    QCOMPARE(info.value, defaultValue);
}

void ParameterTest::passRightArgumentsToTheSaveMethod()
{
    QSettings settings("test.ini", QSettings::IniFormat);

    using TestParameter = Parameter<int, MockIntStrategy>;

    MockIntStrategy::Info info;
    const QString name = "ParamName";
    const int defaultValue = 13;
    const int savedValue = 100;

    TestParameter param(settings,
                        name,
                        defaultValue,
                        MockIntStrategy(info, 0, false));

    param(savedValue);

    QCOMPARE(info.settings, &settings);
    QCOMPARE(info.name, name);
    QCOMPARE(info.value, savedValue);
}

void ParameterTest::returnValueFromTheLoadMethod_data()
{
    QTest::addColumn<int>("value");

    QTest::newRow("-23") << -23;
    QTest::newRow("0") << 0;
    QTest::newRow("324") << 324;
}

void ParameterTest::returnValueFromTheLoadMethod()
{
    QFETCH(int, value);

    QSettings settings("test.ini", QSettings::IniFormat);

    using TestParameter = Parameter<int, MockIntStrategy>;

    MockIntStrategy::Info info;

    TestParameter param(settings,
                        "ParamName",
                        13,
                        MockIntStrategy(info, value, false));

    QCOMPARE(param(), value);
}

void ParameterTest::returnResultFromTheSaveMethod_data()
{
    QTest::addColumn<bool>("result");

    QTest::newRow("false") << false;
    QTest::newRow("true") << true;
}

void ParameterTest::returnResultFromTheSaveMethod()
{
    QFETCH(bool, result);

    QSettings settings("test.ini", QSettings::IniFormat);

    using TestParameter = Parameter<int, MockIntStrategy>;

    MockIntStrategy::Info info;

    TestParameter param(settings,
                        "ParamName",
                        13,
                        MockIntStrategy(info, 0, result));

    QCOMPARE(param(12), result);

}

void ParameterTest::defaultStrategySave()
{
    QStringList expected {
        "[General]",
        "EnumValue=65",
        "IntValue=13",
        "QStringValue=Some String",
    };

    {
        QSettings settings(iniFile, QSettings::IniFormat);

        Parameter<int> intValue(settings, "IntValue");
        Parameter<QString> qstringValue(settings, "QStringValue");
        Parameter<Qt::WindowType> enumValue(settings, "EnumValue", Qt::Desktop);

        QVERIFY(intValue(13));
        QVERIFY(qstringValue("Some String"));
        QVERIFY(enumValue(Qt::CoverWindow));
    }

    QStringList got = readIniFile();

    QVERIFY2(got == expected, makeErrorMessage(got, expected).c_str());
}

void ParameterTest::defaultStrategyLoad()
{
    writeIniFile({"[General]",
                  "EnumValue=65",
                  "IntValue=13",
                  "QStringValue=Some String"
                 });

    {
        QSettings settings(iniFile, QSettings::IniFormat);

        Parameter<int> intValue(settings, "IntValue");
        Parameter<QString> qstringValue(settings, "QStringValue");
        Parameter<Qt::WindowType> enumValue(settings, "EnumValue", Qt::Desktop);

        QCOMPARE(intValue(), 13);
        QCOMPARE(qstringValue(), QString("Some String"));
        QCOMPARE(enumValue(), Qt::CoverWindow);
    }
}

void ParameterTest::defaultStrategyLoadNonexistent()
{
    QSettings settings(iniFile, QSettings::IniFormat);

    Parameter<int> intValue(settings, "IntValue", 131313);
    Parameter<QString> qstringValue(settings, "QStringValue", "<None>");
    Parameter<Qt::WindowType> enumValue(settings, "EnumValue", Qt::Desktop);

    QCOMPARE(intValue(), 131313);
    QCOMPARE(qstringValue(), QString("<None>"));
    QCOMPARE(enumValue(), Qt::Desktop);
}

void ParameterTest::defaultStrategyLoadWithCheck_data()
{
    QTest::addColumn<int>("defaultValue");
    QTest::addColumn<int>("value");
    QTest::addColumn<bool>("checkResult");
    QTest::addColumn<int>("expected");

    QTest::newRow("Fail Validation") << 13 << 100 << false << 13;
    QTest::newRow("Pass Validation") << 13 << 100 << true << 100;
}

void ParameterTest::defaultStrategyLoadWithCheck()
{
    QFETCH(int, defaultValue);
    QFETCH(int, value);
    QFETCH(bool, checkResult);
    QFETCH(int, expected);

    const QString name = "TestValue";
    QSettings settings(iniFile, QSettings::IniFormat);
    settings.setValue(name, value);

    MockIntValidationStrategy::Info info;
    using Strategy = DefaultParameterStrategy<int, MockIntValidationStrategy>;
    Strategy strategy(MockIntValidationStrategy(info, checkResult));

    int got = strategy.load(settings, name, defaultValue);

    QCOMPARE(info.method, MockIntValidationStrategy::Method::Check);
    QCOMPARE(info.value, value);
    QCOMPARE(got, expected);
}

void ParameterTest::defaultStrategySaveWithCheck_data()
{
    QTest::addColumn<int>("value");
    QTest::addColumn<bool>("checkResult");
    QTest::addColumn<bool>("expected");

    QTest::newRow("Fail Validation") << 100 << false << false;
    QTest::newRow("Pass Validation") << 100 << true << true;
}

void ParameterTest::defaultStrategySaveWithCheck()
{
    QFETCH(int, value);
    QFETCH(bool, checkResult);
    QFETCH(bool, expected);

    const QString name = "TestValue";
    QSettings settings(iniFile, QSettings::IniFormat);

    MockIntValidationStrategy::Info info;
    using Strategy = DefaultParameterStrategy<int, MockIntValidationStrategy>;
    Strategy strategy(MockIntValidationStrategy(info, checkResult));

    bool got = strategy.save(settings, name, value);

    QCOMPARE(info.method, MockIntValidationStrategy::Method::Check);
    QCOMPARE(info.value, value);
    QCOMPARE(got, expected);
    QCOMPARE(settings.contains(name), expected);
    if (expected)
    {
        bool ok = false;
        int got = settings.value(name).toInt(&ok);
        QVERIFY(ok);
        QCOMPARE(got, value);
    }
}

void ParameterTest::qenumStrategySave()
{
    QStringList expected {
        "[General]",
        "QEnumValue=CoverWindow"
    };

    {
        QSettings settings(iniFile, QSettings::IniFormat);

        using SortOrderParameter = Parameter<Qt::WindowType, QEnumParameterStrategy<Qt::WindowType>>;
        SortOrderParameter qenumValue(settings, "QEnumValue", Qt::Desktop);

        QVERIFY(qenumValue(Qt::CoverWindow));
    }

    QStringList got = readIniFile();

    QVERIFY2(got == expected, makeErrorMessage(got, expected).c_str());
}

void ParameterTest::qenumStrategyLoad()
{
    writeIniFile({"[General]",
                  "SortOrder=CoverWindow"
                 });

    QSettings settings(iniFile, QSettings::IniFormat);

    using SortOrderParameter = Parameter<Qt::WindowType, QEnumParameterStrategy<Qt::WindowType>>;
    SortOrderParameter qenumValue(settings, "SortOrder", Qt::Desktop);

    QCOMPARE(qenumValue(), Qt::CoverWindow);
}

enum SomeEnum {val1, val2};

void ParameterTest::qenumStrategyLoadNonexistent()
{
    QSettings settings(iniFile, QSettings::IniFormat);

    using SortOrderParameter = Parameter<Qt::WindowType, QEnumParameterStrategy<Qt::WindowType>>;
    SortOrderParameter qenumValue(settings, "SortOrder", Qt::Desktop);

    QCOMPARE(qenumValue(), Qt::Desktop);
}

#if defined(QT_NO_DEBUG) && !defined(QT_FORCE_ASSERTS)
void ParameterTest::qenumStrategyLoadWrong()
{
    writeIniFile({"[General]",
                  "SortOrder=WrongWindowType"
                 });

    QSettings settings(iniFile, QSettings::IniFormat);

    using SortOrderParameter = Parameter<Qt::WindowType, QEnumParameterStrategy<Qt::WindowType>>;
    SortOrderParameter qenumValue(settings, "SortOrder", Qt::Desktop);

    QCOMPARE(qenumValue(), Qt::Desktop);
}
#endif

void ParameterTest::keySetStrategySave_data()
{
    QTest::addColumn<int>("value");
    QTest::addColumn<QString>("saved");
    QTest::addColumn<bool>("result");

    QTest::newRow("1") << 1 << "one" << true;
    QTest::newRow("2") << 2 << "two" << true;
    QTest::newRow("3") << 3 << "three" << true;
    QTest::newRow("Wrong") << 4 << "" << false;
}

void ParameterTest::keySetStrategySave()
{
    QSettings settings(iniFile, QSettings::IniFormat);
    QFETCH(int, value);
    QFETCH(QString, saved);
    QFETCH(bool, result);

    const QString name = "TestValue";
    KeySetParameterStrategy<int> strategy {
        {1, "one"}, {2, "two"}, {3, "three"}
    };

    QCOMPARE(strategy.save(settings, name, value), result);
    QCOMPARE(settings.contains(name), result);
    if (result)
        QCOMPARE(settings.value(name).toString(), saved);
}

void ParameterTest::keySetStrategyLoad_data()
{
    QTest::addColumn<int>("defaultValue");
    QTest::addColumn<int>("value");
    QTest::addColumn<QString>("saved");
    QTest::addColumn<bool>("exists");

    QTest::newRow("1") << 2 << 1 << "one" << true;
    QTest::newRow("2") << 2 << 2 << "two" << true;
    QTest::newRow("3") << 2 << 3 << "three" << true;
    QTest::newRow("Empty") << 2 << 2 << "" << false;
    QTest::newRow("Wrong") << 2 << 2 << "zero" << true;
}

void ParameterTest::keySetStrategyLoad()
{
    QSettings settings(iniFile, QSettings::IniFormat);
    QFETCH(int, defaultValue);
    QFETCH(int, value);
    QFETCH(QString, saved);
    QFETCH(bool, exists);

    const QString name = "TestValue";

    KeySetParameterStrategy<int> strategy {
        {1, "one"}, {2, "two"}, {3, "three"}
    };

    if (exists)
        settings.setValue(name, saved);

    QCOMPARE(strategy.load(settings, name, defaultValue), value);
}

void ParameterTest::urlValidationStrategy_data()
{
    QTest::addColumn<QString>("url");
    QTest::addColumn<bool>("valid");

    //The data for the tests was got from https://mathiasbynens.be/demo/url-regex

    QTest::newRow("http://foo.com/blah_blah - right") << "http://foo.com/blah_blah" << true;
    QTest::newRow("http://foo.com/blah_blah/ - right") << "http://foo.com/blah_blah/" << true;
    QTest::newRow("http://foo.com/blah_blah_(wikipedia) - right") << "http://foo.com/blah_blah_(wikipedia)" << true;
    QTest::newRow("http://foo.com/blah_blah_(wikipedia)_(again) - right") << "http://foo.com/blah_blah_(wikipedia)_(again)" << true;
    QTest::newRow("http://www.example.com/wpstyle/?p=364 - right") << "http://www.example.com/wpstyle/?p=364" << true;
    QTest::newRow("https://www.example.com/foo/?bar=baz&inga=42&quux - right") << "https://www.example.com/foo/?bar=baz&inga=42&quux" << true;
    QTest::newRow("http://✪df.ws/123 - right") << "http://✪df.ws/123" << true;
    QTest::newRow("http://userid:password@example.com:8080 - right") << "http://userid:password@example.com:8080" << true;
    QTest::newRow("http://userid:password@example.com:8080/ - right") << "http://userid:password@example.com:8080/" << true;
    QTest::newRow("http://userid@example.com - right") << "http://userid@example.com" << true;
    QTest::newRow("http://userid@example.com/ - right") << "http://userid@example.com/" << true;
    QTest::newRow("http://userid@example.com:8080 - right") << "http://userid@example.com:8080" << true;
    QTest::newRow("http://userid@example.com:8080/ - right") << "http://userid@example.com:8080/" << true;
    QTest::newRow("http://userid:password@example.com - right") << "http://userid:password@example.com" << true;
    QTest::newRow("http://userid:password@example.com/ - right") << "http://userid:password@example.com/" << true;
    QTest::newRow("http://142.42.1.1/ - right") << "http://142.42.1.1/" << true;
    QTest::newRow("http://142.42.1.1:8080/ - right") << "http://142.42.1.1:8080/" << true;
    QTest::newRow("http://➡.ws/䨹 - right") << "http://➡.ws/䨹" << true;
    QTest::newRow("http://⌘.ws - right") << "http://⌘.ws" << true;
    QTest::newRow("http://⌘.ws/ - right") << "http://⌘.ws/" << true;
    QTest::newRow("http://foo.com/blah_(wikipedia)#cite-1 - right") << "http://foo.com/blah_(wikipedia)#cite-1" << true;
    QTest::newRow("http://foo.com/blah_(wikipedia)_blah#cite-1 - right") << "http://foo.com/blah_(wikipedia)_blah#cite-1" << true;
    QTest::newRow("http://foo.com/unicode_(✪)_in_parens - right") << "http://foo.com/unicode_(✪)_in_parens" << true;
    QTest::newRow("http://foo.com/(something)?after=parens - right") << "http://foo.com/(something)?after=parens" << true;
    QTest::newRow("http://☺.damowmow.com/ - right") << "http://☺.damowmow.com/" << true;
    QTest::newRow("http://code.google.com/events/#&product=browser - right") << "http://code.google.com/events/#&product=browser" << true;
    QTest::newRow("http://j.mp - right") << "http://j.mp" << true;
    QTest::newRow("ftp://foo.bar/baz - right") << "ftp://foo.bar/baz" << true;
    QTest::newRow("http://foo.bar/?q=Test%20URL-encoded%20stuff - right") << "http://foo.bar/?q=Test%20URL-encoded%20stuff" << true;
    QTest::newRow("http://مثال.إختبار - right") << "http://مثال.إختبار" << true;
    QTest::newRow("http://例子.测试 - right") << "http://例子.测试" << true;
    QTest::newRow("http://उदाहरण.परीक्षा - right") << "http://उदाहरण.परीक्षा" << true;
    QTest::newRow("http://-.~_!$&'()*+,;=:%40:80%2f::::::@example.com - right") << "http://-.~_!$&'()*+,;=:%40:80%2f::::::@example.com" << true;
    QTest::newRow("http://1337.net - right") << "http://1337.net" << true;
    QTest::newRow("http://a.b-c.de - right") << "http://a.b-c.de" << true;
    QTest::newRow("http://223.255.255.254 - right") << "http://223.255.255.254" << true;

    QTest::newRow("http://foo.bar?q=Spaces should be encoded - wrong") << "http://foo.bar?q=Spaces should be encoded" << false;
    QTest::newRow("http:// shouldfail.com - wrong") << "http:// shouldfail.com" << false;
    QTest::newRow(":// should fail - wrong") << ":// should fail" << false;
    QTest::newRow("http://foo.bar/foo(bar)baz quux - wrong") << "http://foo.bar/foo(bar)baz quux" << false;
    QTest::newRow("http://-error-.invalid/ - wrong") << "http://-error-.invalid/" << false;
    QTest::newRow("http://-a.b.co - wrong") << "http://-a.b.co" << false;
    QTest::newRow("http://a.b-.co - wrong") << "http://a.b-.co" << false;
    QTest::newRow("http://.www.foo.bar/ - wrong") << "http://.www.foo.bar/" << false;
}

void ParameterTest::urlValidationStrategy()
{
    QFETCH(QString, url);
    QFETCH(bool, valid);

    UrlValidationStrategy strategy;
    QCOMPARE(strategy.check(url), valid);
}

void ParameterTest::fileExistsValidationStrategy_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<bool>("exist");

    QTest::newRow("Empty") << "" << true;
    //The iniFile are removed before each test
    QTest::newRow("Test Ini File") << iniFile << false;
}

void ParameterTest::fileExistsValidationStrategy()
{
    QFETCH(QString, fileName);
    QFETCH(bool, exist);

    const FileExistsValidationStrategy strategy;
    QCOMPARE(strategy.check(fileName), exist);
}

void ParameterTest::fileExistsValidationStrategy_exists()
{
    QSettings settings(iniFile, QSettings::IniFormat);
    settings.setValue("TestValue", "some value");
    settings.sync();

    const FileExistsValidationStrategy strategy;
    QVERIFY(strategy.check(iniFile));
}

QTEST_APPLESS_MAIN(ParameterTest)

#include "tst_parametertest.moc"
