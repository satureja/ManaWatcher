#ifndef AUXILIARY_H
#define AUXILIARY_H

#include <QFile>
#include <QTextStream>
#include <QStringList>

namespace auxiliary
{

QTextStream& operator<<(QTextStream & out, const QStringList & list);
std::string makeErrorMessage(const QStringList & got, const QStringList & expected);

}//namespace auxiliary

#endif // AUXILIARY_H
