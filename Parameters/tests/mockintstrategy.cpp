#include "mockintstrategy.h"

namespace auxiliary
{

MockIntStrategy::MockIntStrategy(Info &info, int loadResult, bool saveResult):
    info(info),
    loadResult(loadResult),
    saveResult(saveResult)
{

}

MockIntStrategy::MockIntStrategy(MockIntStrategy &&that):
    info(that.info),
    loadResult(that.loadResult),
    saveResult(that.saveResult)
{

}

int MockIntStrategy::load(QSettings & settings, const QString &name, const int &defaultValue) const
{
    this->info.method = Method::Load;
    this->info.settings = &settings;
    this->info.name = name;
    this->info.value = defaultValue;

    return this->loadResult;
}

bool MockIntStrategy::save(QSettings & settings, const QString &name, const int & value)
{
    this->info.method = Method::Save;
    this->info.settings = &settings;
    this->info.name = name;
    this->info.value = value;

    return this->saveResult;
}

} //namespace auxiliary
