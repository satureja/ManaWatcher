#ifndef MOCKINTVALIDATIONSTRATEGY_H
#define MOCKINTVALIDATIONSTRATEGY_H

#include <QObject>

namespace auxiliary
{

class MockIntValidationStrategy : public QObject
{
    Q_OBJECT
public:
    enum class Method {None, Check};
    Q_ENUM(Method);

    struct Info
    {
        int value = 0;
        Method method = Method::None;
    };

    MockIntValidationStrategy(Info & info, bool checkResult);
    MockIntValidationStrategy(MockIntValidationStrategy && that);

    bool check(const int & value) const;

private:
    Info & info;
    const bool checkResult;
};

} //namespace auxiliary

#endif // MOCKINTVALIDATIONSTRATEGY_H
