#include "relationresolver.h"

#include <QFile>
#include <QVariant>
#include <QXmlStreamReader>
#include <QDebug>
#include <QThread>

const int friendFlag = 1;
const int enemyFlag = 6;
const int delaySeconds = 3;

RelationResolver::RelationResolver(QObject *parent) : QObject(parent)
{
    QObject::connect(&this->watcher, SIGNAL(fileChanged(const QString&)), this, SLOT(waitAndRead()));
}

void RelationResolver::setPath(QString path)
{
    if (this->path == path)
        return;

    if (!this->path.isEmpty())
        this->watcher.removePath(this->path);

    this->path = path;

    this->watcher.addPath(this->path);
}

void RelationResolver::waitAndRead()
{
    // WARN: Config file may be flushed too late, so we need to wait

    QThread::sleep(delaySeconds);
    read();
}

void RelationResolver::read()
{
    Q_ASSERT(!this->path.isEmpty());

    const char * parseErrorMessage = "Config file parse problem";
    const char * openErrorMessage = "Can not open config file";

    QStringList friends, enemies;

    try
    {
        /* Открываем файл для Чтения с помощью пути, указанного в lineEditWrite */
        QFile file(this->path);
        if (!file.open(QFile::ReadOnly | QFile::Text))
             throw std::runtime_error(openErrorMessage);
         else
        {
            /* Создаем объект, с помощью которого осуществляется чтение из файла */
            QXmlStreamReader xmlReader;
            xmlReader.setDevice(&file);
            xmlReader.readNext();   // Переходит к первому элементу в файле

            auto checkElement = [&] {
               if (!xmlReader.isStartElement() || xmlReader.atEnd())
                   throw std::runtime_error(parseErrorMessage);
            };

            while (!xmlReader.atEnd() && "configuration" != xmlReader.name())
                xmlReader.readNext();

            checkElement();

            while (!xmlReader.atEnd() && !("list" == xmlReader.name() && xmlReader.isStartElement() && xmlReader.attributes().value("name") == "player"))
                xmlReader.readNext();

            checkElement();

            bool recordStarted = false;
            QString lastPlayer;
            int flag = 1;

            do
            {
                xmlReader.readNext();

                if (xmlReader.name() == "player")
                {
                    if (xmlReader.isStartElement())
                        recordStarted = true;
                    else if (xmlReader.isEndElement())
                    {
                        if (!recordStarted || lastPlayer.isEmpty())
                            throw std::runtime_error(parseErrorMessage);

                        if (friendFlag == flag)
                            friends.append(lastPlayer);
                        else if (enemyFlag == flag)
                            enemies.append(lastPlayer);

                        recordStarted = false;
                    }
                    else
                        Q_UNREACHABLE();

                } else if (xmlReader.name() == "option" && xmlReader.isStartElement()) {
                    if (!recordStarted)
                        throw std::runtime_error(parseErrorMessage);

                    QXmlStreamAttributes attrs = xmlReader.attributes();

                    QString name = attrs.value("name").toString();

                    if (name == "name")
                    {
                        lastPlayer = attrs.value("value").toString();
                    }
                    else if (name == "relation")
                    {
                        bool ok = false;

                        flag = attrs.value("value").toInt(&ok);

                        if (!ok)
                            throw std::runtime_error(parseErrorMessage);
                    }
                    else
                        throw std::runtime_error(parseErrorMessage);
                }
            } while(!xmlReader.atEnd() && !("list" == xmlReader.name() && xmlReader.isEndElement()));

            file.close(); // Закрываем файл
        }
    }
    catch (const std::runtime_error & error)
    {
        qCritical() << error.what();
    }

    emit friendsResolved(friends);
    emit enemiesResolved(enemies);
}
