# ManaWatcher buddy
Simple utility to watch when your mana world's friend's log in and log out. Just open configuration window, put there url of server (https://updates.themanaworld.org/updates/online.txt) and select mana plus config.xml. 
For now works only with legacy server themanaworld.org. News about login's and logout's would be shown in tray near application icon.

First positional argument of tool is config path. You can use -t switch to start application in tray, without displaying of main window

# How to build

### Install prerequisites

The first you should check that you have all neccessary dependency. You should have git, Qt 5.6 (or newer), boost. You can install this packages in Ubuntu with follow command:

```
sudo apt install git build-essential libfontconfig1 mesa-common-dev libgl-dev libboost-date-time-dev openssl
```

You can install Qt from main repository with Ubuntu 17.04 (or newer)

```
sudo apt install qt5-default
```

otherwise you should get installer from [official site](https://www.qt.io/download-qt-installer) and install Qt 5.6.3 or newer.

### Getting sources

For getting sources you should clone main repository

```
git clone https://gitlab.com/satureja/ManaWatcher.git
```

The ManaWatcher use git submodules, for getting it you should run follow command:

```
git submodule init && git submodule update
```

### Build

If you install package qt5-default you can run

```
qmake
```

if you install Qt from site the command depends on where you installed it. If you install as root under Ubuntu 64bit you can run qmake with follow command (for Qt 5.6.3 if you use newer just change the version number in the path):

```
/opt/Qt/5.6.3/gcc_64/bin/qmake
```

If all is ok this command doesn't output any to console.

The last command what you should run:

```
make
```

if all is ok you will get the binary file "ManaWatcher".
