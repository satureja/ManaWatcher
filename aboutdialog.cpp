#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    Q_CHECK_PTR(this->ui);

    this->ui->setupUi(this);

    setFixedSize(size());
}

AboutDialog::~AboutDialog()
{
    Q_CHECK_PTR(this->ui);

    delete this->ui;
}
