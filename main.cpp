#include "mainwindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <iostream>

#include "definitions.h"
#include "singleapplication.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static QMap<QtMsgType, QMessageBox::Icon> iconMap;

    if(iconMap.isEmpty())
    {
        iconMap.insert(QtInfoMsg, QMessageBox::Information);
        iconMap.insert(QtWarningMsg, QMessageBox::Warning);
        iconMap.insert(QtCriticalMsg, QMessageBox::Critical);
    }

    QString output = qFormatLogMessage(type, context, msg);

    std::cout << output.toStdString();

    if (QtDebugMsg == type)
        return;

    // To skip unexpected messages
    if (context.file == nullptr || context.function == nullptr)
        return;

    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.setStandardButtons(QtWarningMsg == type ? QMessageBox::Ok | QMessageBox::Abort : QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);

    msgBox.setIcon(iconMap[type]);

    int retcode = msgBox.exec();

    if (QMessageBox::Abort == retcode || QtFatalMsg == type || QtCriticalMsg == type)
        QApplication::exit(1);
}

int main(int argc, char *argv[])
{
    qSetMessagePattern("%{type} (%{file}:%{line}): %{message} - %{time process}");
    qInstallMessageHandler(myMessageOutput);

   // QApplication::setDesktopSettingsAware(false);

    SingleApplication a(argc, argv);

    SingleApplication::setApplicationName(applicationName);
    SingleApplication::setApplicationVersion(applicationVersion);

    if (!a.isPrimary())
        return EXIT_FAILURE;

    QCommandLineParser parser;
    parser.setApplicationDescription("ManaWatcher command line");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("config", "Config file");

    QCommandLineOption showInTray("t", "Show in tray");
    parser.addOption(showInTray);

    QString configPath = QDir::currentPath() + QDir::separator() + defaultConfigFileName;

    if (!parser.parse(a.arguments()))
        return EXIT_FAILURE;

    QStringList arguments = parser.positionalArguments();

    if (1 == arguments.count())
        configPath = arguments.at(0);

    bool isTrayed = parser.isSet(showInTray);

    MainWindow w(configPath);

    if (!isTrayed)
        w.show();

    return a.exec();
}
