#include "configdialog.h"
#include "ui_configdialog.h"

#include <boost/numeric/conversion/cast.hpp>
#include <boost/numeric/conversion/converter_policies.hpp>
#include <QFileInfo>
#include <QDebug>
#include <QStandardPaths>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>

ConfigDialog::ConfigDialog(BasicSettingsInMemory & settings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDialog),
    settings(settings)
{
    Q_CHECK_PTR(this->ui);

    this->ui->setupUi(this);

    setFixedSize(size());

    initPeriods();

    load();
}

ConfigDialog::~ConfigDialog()
{
    Q_CHECK_PTR(this->ui);

    delete this->ui;
}

void ConfigDialog::accept()
{
    if (save())
        QDialog::accept();
}

void ConfigDialog::closeEvent(QCloseEvent * event)
{
    if (this->result() == QDialog::Accepted)
    {
        if (save())
            event->accept();
        else
            event->ignore();
    }
    else
    {
        event->accept();
    }
}

void ConfigDialog::initPeriods()
{
    Q_CHECK_PTR(this->ui);

    for (int i = 0; i < this->ui->comboPeriod->count(); ++i)
    {
        QString value = this->ui->comboPeriod->itemText(i);

        QRegExp rx("^(\\d+(?:.\\d+)?) (hour|minute)$");
        rx.setPatternSyntax(QRegExp::RegExp2);

        bool matched = rx.exactMatch(value);
        Q_ASSERT(matched);

        QStringList list = rx.capturedTexts();

        Q_ASSERT(3 == list.size());

        QString designation = list.at(2);
        bool ok = false;
        float number =  list.at(1).toFloat(&ok);

        Q_ASSERT(ok);

        if (designation == "minute")
            ; // No action
        else if (designation == "hour")
            number *= 60;
        else
            Q_UNREACHABLE();

        using boost::numeric_cast;

        using boost::numeric::bad_numeric_cast;
        using boost::numeric::positive_overflow;
        using boost::numeric::negative_overflow;

        try
        {
            this->ui->comboPeriod->setItemData(i, numeric_cast<int>(number));
        }
        catch(bad_numeric_cast& e) {
           Q_UNREACHABLE();
        }
    }
}

template<typename Parameter, typename Widget, typename Value>
bool saveValue(ConfigDialog * dialog, Parameter & parameter,
               Widget * widget, const Value & value,
               const QString & message)
{
    if (parameter(value))
        return true;

    QMessageBox mb(QMessageBox::Warning,
                applicationName,
                message,
                QMessageBox::Ok,
                dialog);
    widget->setFocus();
    mb.exec();
    return false;
}

bool ConfigDialog::save()
{
    return saveValue(this, this->settings.url, this->ui->lineURL,
                     this->ui->lineURL->text(), "Check the url")
           && saveValue(this, this->settings.config, this->ui->lineConfig,
                        this->ui->lineConfig->text(), "Check the config")
           && saveValue(this, this->settings.period, this->ui->comboPeriod,
                        this->ui->comboPeriod->currentData().toInt(), "Check the period");
}

bool ConfigDialog::load()
{
    this->ui->lineURL->setText(this->settings.url());
    this->ui->lineConfig->setText(this->settings.config());
    return this->setPeriod(this->settings.period());
}

bool ConfigDialog::setPeriod(int period)
{
    Q_CHECK_PTR(this->ui);

    int counter = 0;

    for (; counter < this->ui->comboPeriod->count() && period != this->ui->comboPeriod->itemData(counter); ++counter)
        ; // No action

    if (counter >= this->ui->comboPeriod->count())
    {
        qWarning() << "Invalid period value: " << period;
        return false;
    }

    this->ui->comboPeriod->setCurrentIndex(counter);

    return true;
}

void ConfigDialog::on_buttonLocateConfig_clicked()
{
    Q_CHECK_PTR(this->ui);

    QString startFolder = QStandardPaths::locate(QStandardPaths::ConfigLocation, QString(), QStandardPaths::LocateDirectory);

    QString filePath = QFileDialog::getOpenFileName(this, "Open config File", startFolder, "XML Files (*.xml)");

    if (!filePath.isEmpty())
        this->ui->lineConfig->setText(filePath);
}
