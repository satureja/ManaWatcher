#ifndef PLAYERSUPPLIER_H
#define PLAYERSUPPLIER_H

#include <QObject>

#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkReply>

#include "definitions.h"

class PlayerResolver : public QObject
{
    Q_OBJECT
public:
    explicit PlayerResolver(QObject *parent = 0);
    void setUrl(QString url);

signals:
    void dataReceived(QByteArray data);

public slots:
    void execute();

private slots:
    // Обработчик данных полученных от объекта QNetworkAccessManager
    void onResult(QNetworkReply *reply);

private:
    QNetworkAccessManager *networkManager;
    QUrl url;
};

class PlayerParser : public QObject
{
    Q_OBJECT
public:
    explicit PlayerParser(QObject *parent = 0);

signals:
    void dataParsed(PlayerList data);

public slots:
    void parseData(QByteArray data);
};

class PlayerSupplier : public QObject
{
    Q_OBJECT
public:
    explicit PlayerSupplier(const QString & url, QObject *parent = 0);

public slots:
    void request();
    void setUrl(const QString & url);

signals:
    void suppply(PlayerList data);

private:
    PlayerResolver * resolver = nullptr;
    PlayerParser * parser = nullptr;
};

#endif // PLAYERSUPPLIER_H
