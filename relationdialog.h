#ifndef RELATIONDIALOG_H
#define RELATIONDIALOG_H

#include "definitions.h"
#include <QDialog>
#include <QStringListModel>

namespace Ui {
class RelationDialog;
}

class RelationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RelationDialog(QWidget *parent = 0);
    ~RelationDialog();

public slots:
    void setEnemies(PlayerList players);
    void setFriends(PlayerList players);

protected:
    void showEvent(QShowEvent *event);

private slots:
    void on_comboBox_currentIndexChanged(int index);

private:
    void updateModel(int index);

private:
    PlayerList friendsPlayers;
    PlayerList enemiesPlayers;

    QStringListModel * currentModel = nullptr;

    Ui::RelationDialog *ui = nullptr;
};

#endif // RELATIONDIALOG_H
