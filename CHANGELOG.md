# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
Settings refactor, binaries for Linux and Windows.

## [0.2.0] - 2018-01-09
### Fixed
- Fixed MainWindow invalid position after window restore
- Fixed players list parsing bug's
- Removed Abort button for critical and fatal messages
- Trimmed tray popup message
- Fixed release version invalid refresh period bug
- Disabled displaying "Keep running" message more then once

## [0.1.0] - 2018-01-03
### Added
- Sources of project were placed in public access

[Unreleased]: https://gitlab.com/satureja/ManaWatcher/compare/v1.6.0...HEAD
[0.2.0]: https://gitlab.com/satureja/ManaWatcher/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/satureja/ManaWatcher/compare/v0.1.0~1...v0.1.0
